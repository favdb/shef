/*
 * Created on Jan 13, 2006
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.dialogs.ImageDialog;
import eu.ostorybook.shef.resources.images.Images;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 * Action which desplays a dialog to insert an image
 *
 * @author favdb
 *
 */
public class SRCImageAction extends SRCAbstractAction {

	public SRCImageAction() {
		super(i18n.str("image_"));
		setSmallIcon(Images.getIcon("image"));
		setShortDescription(i18n.str("image_desc"));
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		ImageDialog d = createDialog(editor);
		//d.setSize(300, 300);
		d.setLocationRelativeTo(d.getParent());
		d.setVisible(true);
		if (d.hasUserCancelled()) {
			return;
		}
		editor.requestFocusInWindow();
		editor.replaceSelection(d.getHTML());
	}

	protected ImageDialog createDialog(JTextComponent ed) {
		Window w = SwingUtilities.getWindowAncestor(ed);
		ImageDialog d = null;
		if (w != null && w instanceof Frame) {
			d = new ImageDialog((Frame) w);
		} else if (w != null && w instanceof Dialog) {
			d = new ImageDialog((Dialog) w);
		}
		return d;
	}

}

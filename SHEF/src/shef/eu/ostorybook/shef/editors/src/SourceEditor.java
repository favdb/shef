/*
 * Copyright (C) oStorybook Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.SHEFEditor;
import eu.ostorybook.shef.actions.manager.ActionList;
import eu.ostorybook.shef.editors.AbstractPanel;
import static eu.ostorybook.shef.editors.AbstractPanel.i18n;
import eu.ostorybook.shef.resources.images.Images;
import java.awt.BorderLayout;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import org.w3c.tidy.Tidy;

/**
 *
 * @author favdb
 */
public class SourceEditor extends AbstractPanel {

	//TODO add syntax highlighter
	
	private JEditorPane ed;
	private final SHEFEditor editor;

	public SourceEditor(SHEFEditor parent) {
		super(parent);
		this.editor = parent;
		setName("source");
		initAll();
	}

	@Override
	public void init() {
		setLayout(new BorderLayout());
		setOpaque(true);
	}

	@Override
	public void initUi() {
		initToolbar();
		add(toolbar, BorderLayout.PAGE_START);
		//TODO ed or scroll may be adjust size to fit space
		ed = new JEditorPane();
		JScrollPane scroll = new JScrollPane(ed);
		add(scroll, BorderLayout.CENTER);
	}

	@Override
	public JToolBar initToolbar() {
		super.initToolbar();
		actionList = new ActionList("editor-actions");
		// button to indent text
		toolbar.add(initButton(actionList, new SRCIndentAction(SRCIndentAction.INDENT)));
		// button to go to Wysiwyg mode
		JButton bt = new JButton(Images.getIcon("wysiwyg"));
		bt.setName("source");
		bt.setText("");
		bt.setToolTipText(i18n.str("wysiwyg"));
		bt.addActionListener(e -> {
			editor.changeTo("wysiwyg");
		});
		toolbar.add(bt);
		return toolbar;
	}

	public void setText(String texte) {
		ed.setText(indent(texte,3));
		ed.setCaretPosition(0);
	}

	public void setCaretPosition(int position) {
		ed.setCaretPosition(position);
	}

	public String getText() {
		return ed.getText();
	}

	public static String indent(String html, int spaces) {
        Tidy tidy = new Tidy();
		tidy.setTrimEmptyElements(true);
		tidy.setXHTML(true);
		tidy.setDropEmptyParas(true);
		tidy.setSmartIndent(true);
		tidy.setSpaces(spaces);
		tidy.setQuiet(true);
		tidy.setShowWarnings(false);
		tidy.setWraplen(80);
		InputStream inputStream = new ByteArrayInputStream(html.getBytes());
		ByteArrayOutputStream buf=new ByteArrayOutputStream();
		tidy.parseDOM(inputStream, buf);
		String r=buf.toString();
		int d=r.indexOf("<body>\n");
		r=r.substring(d+"<body>\n".length());
		int f=r.indexOf("</body>");
		r=r.substring(0,f);
		return r;
	}

}

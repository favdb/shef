/*
 * Copyright (C) 2020 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.actions.ActionDefault;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;

/**
 *
 * @author favdb
 */
public abstract class SRCAbstractAction extends ActionDefault {
	
	public static final String EDITOR = "editor";
	public static final int DISABLED = -1;
	public static final int SOURCE = 1;
	
	public SRCAbstractAction(String name) {
		super(name);
		updateEnabledState();
	}

	protected JEditorPane getCurrentEditor() {
		try {
			JEditorPane ep = (JEditorPane) getContextValue(EDITOR);
			return ep;
		} catch (ClassCastException cce) {
		}

		return null;
	}

	@Override
	public void execute(ActionEvent e) throws Exception {
		sourceEditPerformed(e, getCurrentEditor());
	}

	@Override
	protected void actionPerformedCatch(Throwable t) {
		t.printStackTrace(System.out);
	}

	@Override
	protected void contextChanged() {
		updateSourceContextState(getCurrentEditor());
	}

	protected void updateSourceContextState(JEditorPane srcEditor) {
	}

	protected abstract void sourceEditPerformed(ActionEvent e, JEditorPane editor);

}

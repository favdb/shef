/*
 * Created on Feb 28, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/**
 * Action which clears inline text styles
 *
 * @author Bob Tantlinger
 *
 */
public class SRCClearStyles extends SRCAbstractAction {

	public SRCClearStyles() {
		super(i18n.str("clear_styles"));
		setMnemonic("clear_styles");
		setAccelerator(KeyStroke.getKeyStroke("shift ctrl Y"));
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
	}

}

/*
 * Created on december, 2020
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.dialogs.SpecialCharDialog;
import eu.ostorybook.shef.resources.images.Images;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;

/**
 *  insert HTML incode character
 * 
 * @author favdb
 */
public class SRCUnicodeAction extends SRCAbstractAction {

	SpecialCharDialog dialog;

	public SRCUnicodeAction() {
		super(i18n.str("unicode"));
		setSmallIcon(Images.getIcon("char_unicode"));
		setShortDescription(i18n.str("unicode_desc"));
	}

	protected void doEdit(ActionEvent e, JEditorPane ed) {
		Component c = SwingUtilities.getWindowAncestor(ed);
		if (dialog == null) {
			if (c instanceof Frame) {
				dialog = new SpecialCharDialog((Frame) c, ed);
			} else if (c instanceof Dialog) {
				dialog = new SpecialCharDialog((Dialog) c, ed);
			} else {
				return;
			}
		}
		dialog.setInsertEntity(true);
		if (!dialog.isVisible()) {
			dialog.setLocationRelativeTo(c);
			dialog.setVisible(true);
		}
	}

	protected void updateContextState(JEditorPane editor) {
		if (dialog != null) {
			dialog.setInsertEntity(true);
			dialog.setJTextComponent(editor);
		}
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		doEdit(e, editor);
	}

	@Override
	protected final void updateSourceContextState(JEditorPane srcEditor) {
		updateContextState(srcEditor);
	}
}

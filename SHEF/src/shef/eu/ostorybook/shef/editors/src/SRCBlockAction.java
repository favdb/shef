/*
 * Created on Feb 26, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.html.HTML;

/**
 * Action which formats HTML block level elements
 *
 * @author favdb
 *
 */
public class SRCBlockAction extends SRCAbstractAction {

	public static final int
			DIV = 0,
			P = 1,
			H1 = 2,
			H2 = 3,
			H3 = 4,
			H4 = 5,
			H5 = 6,
			H6 = 7,
			PRE = 8,
			BLOCKQUOTE = 9,
			OL = 10,
			UL = 11;
	private static final int KEYS[] = {
				KeyEvent.VK_D, KeyEvent.VK_ENTER, KeyEvent.VK_1, KeyEvent.VK_2,
				KeyEvent.VK_3, KeyEvent.VK_4, KeyEvent.VK_5, KeyEvent.VK_6,
				KeyEvent.VK_R, KeyEvent.VK_Q, KeyEvent.VK_N, KeyEvent.VK_U
			};
	public static final String[] TYPES = {
				"body_text",
				"paragraph",
				"heading1",
				"heading2",
				"heading3",
				"heading4",
				"heading5",
				"heading6",
				"preformatted",
				"blockquote",
				"listordered",
				"listunordered"
			};
	private int type;

	/**
	 * Creates a new HTMLBlockAction
	 *
	 * @param type A block type - P, PRE, BLOCKQUOTE, H1, H2, etc
	 *
	 * @throws IllegalArgumentException
	 */
	public SRCBlockAction(int type) throws IllegalArgumentException {
		super("");
		if (type < 0 || type >= TYPES.length) {
			throw new IllegalArgumentException("Illegal argument");
		}
		this.type = type;
		putValue(NAME, TYPES[type]);
		setShortDescription(i18n.str(TYPES[type]));
		setAccelerator(KeyStroke.getKeyStroke(KEYS[type], InputEvent.ALT_DOWN_MASK));
		setMnemonic(i18n.mnem(TYPES[type]));
		if (type==OL || type==UL) setSmallIcon(TYPES[type]);
	}

	@Override
	protected void updateSourceContextState(JEditorPane ed) {
		setSelected(false);
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		String tag = getTag().toString();
		String prefix = "\n<" + tag + ">\n\t";
		String postfix = "\n</" + tag + ">\n";
		if (type == OL || type == UL) {
			prefix += "<li>";
			postfix = "</li>" + postfix;
		}
		String sel = editor.getSelectedText();
		if (sel == null) {
			editor.replaceSelection(prefix + postfix);
			int pos = editor.getCaretPosition() - postfix.length();
			if (pos >= 0) {
				editor.setCaretPosition(pos);
			}
		} else {
			sel = prefix + sel + postfix;
			editor.replaceSelection(sel);
		}
	}

	/**
	 * Gets the tag
	 *
	 * @return
	 */
	public HTML.Tag getTag() {
		HTML.Tag tag = HTML.Tag.DIV;
		switch (type) {
			case DIV:
				tag = HTML.Tag.DIV;
				break;
			case P:
				tag = HTML.Tag.P;
				break;
			case H1:
				tag = HTML.Tag.H1;
				break;
			case H2:
				tag = HTML.Tag.H2;
				break;
			case H3:
				tag = HTML.Tag.H3;
				break;
			case H4:
				tag = HTML.Tag.H4;
				break;
			case H5:
				tag = HTML.Tag.H5;
				break;
			case H6:
				tag = HTML.Tag.H6;
				break;
			case PRE:
				tag = HTML.Tag.PRE;
				break;
			case UL:
				tag = HTML.Tag.UL;
				break;
			case OL:
				tag = HTML.Tag.OL;
				break;
			case BLOCKQUOTE:
				tag = HTML.Tag.BLOCKQUOTE;
				break;
		}
		return tag;
	}

}

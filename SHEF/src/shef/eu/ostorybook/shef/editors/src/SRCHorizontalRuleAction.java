/*
 * Created on Mar 3, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JEditorPane;

/**
 * Action which inserts a horizontal rule
 *
 * @author favdb
 *
 */
public class SRCHorizontalRuleAction extends SRCAbstractAction {

	public SRCHorizontalRuleAction() {
		super(i18n.str("horizontal_rule"));
		putValue(MNEMONIC_KEY, i18n.mnem("horizontal_rule"));
		putValue(SMALL_ICON, Images.getIcon("hrule"));
		putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		editor.replaceSelection("<hr>");
	}

}

/*
 * Created on Mar 3, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
/**
 * Action which inserts special characters like cadratin
 *
 * @author favdb
 *
 */
public class SRCCadratinAction extends SRCAbstractAction {

	public static final int
			CADRATIN = 0,
			DIALOG_OPEN_EN = 1,
			DIALOG_CLOSE_EN = 2,
			DIALOG_OPEN_FR = 3,
			DIALOG_CLOSE_FR = 4;
	public static final String[] TYPES = {
				"cadratin",
				"dialog_open_en",
				"dialog_close_en",
				"dialog_open_fr",
				"dialog_close_fr"
			};
	private int type;

	public SRCCadratinAction(int c) {
		super(TYPES[c]);
		setMnemonic(i18n.mnem(TYPES[c]));
		setShortDescription(i18n.str(TYPES[c]));
		setSmallIcon(Images.getIcon(TYPES[c]));
		type = c;
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		switch(type) {
			case 0:
				editor.replaceSelection("&mdash; ");
				break;
			case 1:
				editor.replaceSelection("&#147;&nbsp;");
				break;
			case 2:
				editor.replaceSelection("&nbsp;&#132;");
				break;
			case 3:
				editor.replaceSelection("&#171;&nbsp;");
				break;
			case 4:
				editor.replaceSelection("&nbsp;&#187;");
				break;
		}
	}

}

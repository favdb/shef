/*
 * Created on Feb 25, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;

/**
 * HTML line break action
 * 
 */
public class SRCLineBreakAction extends SRCAbstractAction {

	public SRCLineBreakAction() {
		super(i18n.str("line_break"));
		setMnemonic(i18n.mnem("line_break"));
		setSmallIcon(Images.getIcon("char_br"));
		setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.SHIFT_DOWN_MASK));
		setShortDescription(i18n.str("line_break"));
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		editor.replaceSelection("<br>\n");
	}

}

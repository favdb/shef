/*
 * Created on Jun 16, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.actions.*;
import eu.ostorybook.shef.tools.ElementWriter;
import eu.ostorybook.shef.tools.HTMLUtils;
import java.awt.event.ActionEvent;
import java.io.StringWriter;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;

/**
 *
 * Action for adding and removing table elements
 *
 * @author Bob Tantlinger
 *
 */
public class SRCTableEditAction extends SRCAbstractAction {

	public static final int INSERT_CELL = 0;
	public static final int DELETE_CELL = 1;
	public static final int INSERT_ROW = 2;
	public static final int DELETE_ROW = 3;
	public static final int INSERT_COL = 4;
	public static final int DELETE_COL = 5;
	private static final String NAMES[] = {
				i18n.str("insert_cell"),
				i18n.str("delete_cell"),
				i18n.str("insert_row"),
				i18n.str("delete_row"),
				i18n.str("insert_column"),
				i18n.str("delete_column")
			};
	private int type;

	public SRCTableEditAction(int type) throws IllegalArgumentException {
		super("");
		if (type < 0 || type >= NAMES.length) {
			throw new IllegalArgumentException("Invalid type");
		}
		this.type = type;
		putValue(NAME, NAMES[type]);
		addShouldBeEnabledDelegate((Action a) -> isInTD(getCurrentEditor()));
	}

	private boolean isInTD(JEditorPane tc) {
		Element td = null;
		if (tc != null) {
			HTMLDocument doc = (HTMLDocument) tc.getDocument();
			try {
				Element curElem = doc.getParagraphElement(tc.getCaretPosition());
				td = HTMLUtils.getParent(curElem, HTML.Tag.TD);
			} catch (Exception ex) {
			}
		}
		return td != null;
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
	}

}

/*
 * Created on Feb 25, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.text.html.HTML;

/**
 * Action which toggles inline HTML elements
 *
 * @author favdb
 *
 */
public class SRCHighlightAction extends SRCAbstractAction {
	
	public static String htmlEM="<em class=\"highlight\" style=\"backgroung:yellow\">";

	public static final int EM = 0;

	public static final String TYPES = "emphasis";

	private int type;

	/**
	 * Creates a new HTMLInlineAction
	 *
	 * @param itype an inline element type (BOLD, ITALIC, STRIKE, etc)
	 * @throws IllegalArgumentException
	 */
	public SRCHighlightAction() throws IllegalArgumentException {
		super(TYPES);
		type = EM;
		putValue(NAME, TYPES);
		setMnemonic(i18n.mnem(TYPES));
		this.setShortDescription(i18n.str(TYPES));
		setSmallIcon(Images.getIcon("highlighter"));
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		String prefix = htmlEM;
		String postfix = "</em>";
		String sel = editor.getSelectedText();
		if (sel == null) {
			editor.replaceSelection(prefix+postfix);
			int pos = editor.getCaretPosition() - postfix.length();
			if (pos >= 0) {
				editor.setCaretPosition(pos);
			}
		} else {
			sel = prefix + sel + postfix;
			editor.replaceSelection(sel);
		}
	}

	public HTML.Tag getTag() {
		return HTML.Tag.EM;
	}

	@Override
	protected void updateSourceContextState(JEditorPane ed) {
		setSelected(false);
	}
}

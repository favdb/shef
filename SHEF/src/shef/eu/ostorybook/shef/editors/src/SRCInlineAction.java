/*
 * Created on Feb 25, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.actions.*;
import eu.ostorybook.shef.resources.images.Images;
import eu.ostorybook.shef.tools.HTMLUtils;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.Icon;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.AttributeSet;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.CSS;
import javax.swing.text.html.HTML;

/**
 * Action which toggles inline HTML elements
 *
 * @author favdb
 *
 */
public class SRCInlineAction extends SRCAbstractAction {

	public static final int
			EM = 0,
			STRONG = 1,
			CODE = 2,
			CITE = 3,
			SUP = 4,
			SUB = 5,
			BOLD = 6,
			ITALIC = 7,
			UNDERLINE = 8,
			STRIKE = 9;

	public static final String[] TYPES = {
				"emphasis",
				"strong",
				"code",
				"cite",
				"superscript",
				"subscript",
				"bold",
				"italic",
				"underline",
				"strike"
			};

	private int type;

	/**
	 * Creates a new HTMLInlineAction
	 *
	 * @param itype an inline element type (BOLD, ITALIC, STRIKE, etc)
	 * @throws IllegalArgumentException
	 */
	public SRCInlineAction(int itype) throws IllegalArgumentException {
		super("");
		type = itype;
		if (type < 0 || type >= TYPES.length) {
			throw new IllegalArgumentException("Illegal Argument");
		}
		putValue(NAME, TYPES[type]);
		setMnemonic(i18n.mnem(TYPES[type]));
		this.setShortDescription(i18n.str(TYPES[type]));

		Icon ico = null;
		KeyStroke ks = null;
		switch (type) {
			case BOLD:
				ico = Images.getIcon("text_bold");
				ks = KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_DOWN_MASK);
				break;
			case ITALIC:
				ico = Images.getIcon("text_italic");
				ks = KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK);
				break;
			case UNDERLINE:
				ico = Images.getIcon("text_underline");
				ks = KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_DOWN_MASK);
				break;
			case STRIKE:
				ico = Images.getIcon("text_strike");
				ks = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK);
				break;
			case SUB:
				ico = Images.getIcon("text_subscript");
				break;
			case SUP:
				ico = Images.getIcon("text_superscript");
				break;
			default:
				break;
		}
		setSmallIcon(ico);
		setAccelerator(ks);
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		HTML.Tag tag = getTag();
		String prefix = "<" + tag.toString() + ">";
		String postfix = "</" + tag.toString() + ">";
		String sel = editor.getSelectedText();
		if (sel == null) {
			editor.replaceSelection(prefix + postfix);
			int pos = editor.getCaretPosition() - postfix.length();
			if (pos >= 0) {
				editor.setCaretPosition(pos);
			}
		} else {
			sel = prefix + sel + postfix;
			editor.replaceSelection(sel);
		}
	}

	public HTML.Tag getTag() {
		return getTagForType(type);
	}

	private HTML.Tag getTagForType(int type) {
		HTML.Tag tag = null;
		switch (type) {
			case EM:
				tag = HTML.Tag.EM;
				break;
			case STRONG:
				tag = HTML.Tag.STRONG;
				break;
			case CODE:
				tag = HTML.Tag.CODE;
				break;
			case SUP:
				tag = HTML.Tag.SUP;
				break;
			case SUB:
				tag = HTML.Tag.SUB;
				break;
			case CITE:
				tag = HTML.Tag.CITE;
				break;
			case BOLD:
				tag = HTML.Tag.B;
				break;
			case ITALIC:
				tag = HTML.Tag.I;
				break;
			case UNDERLINE:
				tag = HTML.Tag.U;
				break;
			case STRIKE:
				tag = HTML.Tag.STRIKE;
				break;
		}
		return tag;
	}

	@Override
	protected void updateSourceContextState(JEditorPane ed) {
		setSelected(false);
	}
}

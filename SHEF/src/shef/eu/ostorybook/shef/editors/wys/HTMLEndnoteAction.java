/*
 * Created on Feb 25, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.SHEFEditor;
import eu.ostorybook.shef.actions.Endnote;
import eu.ostorybook.shef.resources.images.Images;
import eu.ostorybook.shef.tools.HTMLUtils;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.html.HTML;

/**
 *
 */
public class HTMLEndnoteAction extends HTMLTextEditAction {

	private List<Endnote> endnotes;
	private int number;
	private final SHEFEditor editor;
	private final WysiwygEditor wysEditor;

	public HTMLEndnoteAction(SHEFEditor editor, WysiwygEditor wysEditor) {
		super(i18n.str("endnote"));
		this.editor=editor;
		this.wysEditor=wysEditor;
		setSmallIcon(Images.getIcon("endnote"));
		setShortDescription(i18n.str("endnote"));
	}

	@Override
	@SuppressWarnings("null")
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane ed) {
		//Log.write("HtmlEndnodeAction.wysiwygEditPerformed(e=" + e.toString() + ", ed=" + ed.toString() + ")");
		Window w = SwingUtilities.getWindowAncestor(ed);
		while (true) {
			if (w instanceof Frame) {
				break;
			}
			w = w.getOwner();
			if (w == null) {
				return;
			}
		}
		//TODO Endnote change to call a new WysiwygEditor
		WysiwygEditor wys = new WysiwygEditor(wysEditor);
		JDialog dlg=new JDialog();
		dlg.add(wys);
		dlg.pack();
		dlg.setVisible(true);
		HTMLUtils.insertHTML(Endnote.getLinkToEndnote(wys.getText()), HTML.Tag.A, ed);
	}

}

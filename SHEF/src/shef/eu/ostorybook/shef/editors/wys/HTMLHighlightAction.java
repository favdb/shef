/*
 * Created on Feb 25, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.actions.CompoundUndoManager;
import eu.ostorybook.shef.resources.images.Images;
import eu.ostorybook.shef.tools.HTMLUtils;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.CSS;
import javax.swing.text.html.HTML;

/**
 * Action which toggles inline HTML elements
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLHighlightAction extends HTMLTextEditAction {
	
	public static String htmlTag="<em class=\"highlight\" style=\"backgroung:yellow\">";

	public static final int EM = 0;

	public static final String TYPES = "emphasis";

	private int type;

	/**
	 * Creates a new HTMLInlineAction
	 *
	 * @param itype an inline element type (BOLD, ITALIC, STRIKE, etc)
	 * @throws IllegalArgumentException
	 */
	public HTMLHighlightAction() throws IllegalArgumentException {
		super(TYPES);
		type = EM;
		putValue(NAME, TYPES);
		setMnemonic(i18n.mnem(TYPES));
		this.setShortDescription(i18n.str(TYPES));
		setSmallIcon(Images.getIcon("highlighter"));
	}

	@Override
	protected void updateWysiwygContextState(JEditorPane ed) {
		setSelected(isDefined(HTMLUtils.getCharacterAttributes(ed)));
	}

	public HTML.Tag getTag() {
		return HTML.Tag.EM;
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		CompoundUndoManager.beginCompoundEdit(editor.getDocument());
		toggleStyle(editor);
		CompoundUndoManager.endCompoundEdit(editor.getDocument());
	}

	private boolean isDefined(AttributeSet attr) {
		boolean hasSC = StyleConstants.getBackground(attr).equals(Color.YELLOW);
		return hasSC;
	}

	private void toggleStyle(JEditorPane editor) {
		MutableAttributeSet attr = new SimpleAttributeSet();
		attr.addAttributes(HTMLUtils.getCharacterAttributes(editor));
		boolean enable = !isDefined(attr);
		if (enable) {
			MutableAttributeSet cattr = new SimpleAttributeSet();
			cattr.addAttributes(HTMLUtils.getCharacterAttributes(editor));
			StyleConstants.setBackground(cattr, Color.YELLOW);
			//doesn't replace any attribs, just adds the new one
			HTMLUtils.setCharacterAttributes(editor, cattr);
			AttributeSet battr = HTMLUtils.getCharacterAttributes(editor);
			System.out.println("color="+StyleConstants.getBackground(battr).toString());
		} else {
			//Kind of a ham-fisted way to do this, but sometimes there are
			//CSS attributes, someties there are HTML.Tag attributes, and sometimes
			//there are both. So, we have to remove 'em all to make sure this type
			//gets completely disabled
			//remove the CSS style
			//STRONG, EM, CITE, CODE have no CSS analogs
			switch (type) {
				case EM:
					HTMLUtils.removeCharacterAttribute(editor, CSS.Attribute.BACKGROUND, "em");
					break;
				default:
					break;
			}
			//make certain the tag is also removed
			HTMLUtils.removeCharacterAttribute(editor, HTML.Tag.EM);
			AttributeSet battr = HTMLUtils.getCharacterAttributes(editor);
			System.out.println("color="+StyleConstants.getBackground(battr).toString());
			System.out.println(editor.getText());
		}
		setSelected(enable);
	}

}

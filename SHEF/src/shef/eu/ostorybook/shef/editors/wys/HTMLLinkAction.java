/*
 * Created on Feb 26, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.dialogs.HyperlinkDialog;
import eu.ostorybook.shef.resources.images.Images;
import eu.ostorybook.shef.tools.HTMLUtils;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import javax.swing.text.html.HTML;

/**
 * Action which displays a dialog to insert a hyperlink
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLLinkAction extends HTMLTextEditAction {

	public HTMLLinkAction() {
		super(i18n.str("hyperlink_"));
		setMnemonic(i18n.mnem("hyperlink_"));
		setSmallIcon(Images.getIcon("link"));
		setShortDescription(i18n.str("hyperlink_desc"));
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		HyperlinkDialog dlg = createDialog(editor);
		if (dlg == null) {
			return;
		}
		if (editor.getSelectedText() != null) {
			dlg.setLinkText(editor.getSelectedText());
		}
		dlg.setLocationRelativeTo(dlg.getParent());
		dlg.setVisible(true);
		if (dlg.hasUserCancelled()) {
			return;
		}
		String tagText = dlg.getHTML();
		if (editor.getSelectedText() == null) {
			tagText += "&nbsp;";
		}
		editor.replaceSelection("");
		HTMLUtils.insertHTML(tagText, HTML.Tag.A, editor);
	}

	protected HyperlinkDialog createDialog(JTextComponent ed) {
		Window w = SwingUtilities.getWindowAncestor(ed);
		HyperlinkDialog d = null;
		if (w != null && w instanceof Frame) {
			d = new HyperlinkDialog((Frame) w);
		} else if (w != null && w instanceof Dialog) {
			d = new HyperlinkDialog((Dialog) w);
		}
		return d;
	}

}

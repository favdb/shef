/*
 * Created on Mar 3, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.text.Element;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/**
 * Action which inserts a horizontal rule
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLHorizontalRuleAction extends HTMLTextEditAction {

	public HTMLHorizontalRuleAction() {
		super(i18n.str("horizontal_rule"));
		putValue(MNEMONIC_KEY, i18n.mnem("horizontal_rule"));
		putValue(SMALL_ICON, Images.getIcon("hrule"));
		putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		HTMLDocument document = (HTMLDocument) editor.getDocument();
		int caret = editor.getCaretPosition();
		Element elem = document.getParagraphElement(caret);

		HTML.Tag tag = HTML.getTag(elem.getName());
		if (elem.getName().equals("p-implied")) {
			tag = HTML.Tag.IMPLIED;
		}
		HTMLEditorKit.InsertHTMLTextAction a
				= new HTMLEditorKit.InsertHTMLTextAction("", "<hr>", tag, HTML.Tag.HR);
		a.actionPerformed(e);
	}

}

/*
 * Created on Feb 28, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JColorChooser;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.StyledEditorKit;

/**
 * Action which edits HTML font color
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLFontColorAction extends HTMLTextEditAction {

	public HTMLFontColorAction() {
		super(i18n.str("color_"));
		putValue(MNEMONIC_KEY, i18n.mnem("color_"));
		this.putValue(SMALL_ICON, Images.getIcon("color"));
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		Color color = getColorFromUser(editor);
		if (color != null) {
			Action a = new StyledEditorKit.ForegroundAction("Color", color);
			a.actionPerformed(e);
		}
	}

	private Color getColorFromUser(Component c) {
		Window win = SwingUtilities.getWindowAncestor(c);
		if (win != null) {
			c = win;
		}
		Color color = JColorChooser.showDialog(c, "Color", Color.black);
		return color;
	}

}

/*
 * Copyright (C) oStorybook Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.SHEFEditor;
import eu.ostorybook.shef.actions.ActionDefault;
import eu.ostorybook.shef.actions.CompoundUndoManager;
import eu.ostorybook.shef.tools.HTMLUtils;
import eu.ostorybook.shef.actions.FindReplaceAction;
import static eu.ostorybook.shef.editors.wys.HTMLCadratinAction.*;
import eu.ostorybook.shef.actions.manager.ActionList;
import eu.ostorybook.shef.actions.manager.ActionUIFactory;
import eu.ostorybook.shef.editors.AbstractPanel;
import eu.ostorybook.shef.resources.images.Images;
import eu.ostorybook.shef.tools.Log;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.undo.UndoManager;

/**
 * some codes are inspired by SHEF from Sourceforge by Bob Tantlinger
 * initial adaptation by favdb (2020)
 * 
 */
public class WysiwygEditor extends AbstractPanel {

	private static final String INVALID_TAGS[] = {"html", "head", "body", "title"};
	private JEditorPane wysEditor;
	private JEditorPane focusedEditor;
	private JComboBox fontFamilyCombo;
	private JComboBox paragraphCombo;
	private JPopupMenu wysPopupMenu;
	private FocusListener focusHandler = new FocusHandler();
	private DocumentListener textChangedHandler = new TextChangedHandler();
	private ActionListener fontChangeHandler = new FontChangeHandler();
	private ActionListener paragraphComboHandler = new ParagraphComboHandler();
	private CaretListener caretHandler = new CaretHandler();
	private MouseListener popupHandler = new PopupHandler();
	private boolean isTextChanged=false;
	private boolean reduced;
	private int dlg_lang = 0;//for dialog open/close, 0=all, 1=english only, 2=french only
	private SHEFEditor editor;
	private JButton btEndnote;

	public WysiwygEditor(SHEFEditor editor, int dlg_lang, boolean reduced) {
		super(editor);
		this.editor=editor;
		this.dlg_lang=dlg_lang;
		this.reduced = reduced;
		initUI();
	}

	public WysiwygEditor(WysiwygEditor parent) {
		super();
		this.editor=null;
		this.dlg_lang=parent.dlg_lang;
		this.reduced=parent.reduced;
	}

	public void setCaretPosition(int pos) {
		wysEditor.setCaretPosition(pos);
		wysEditor.requestFocusInWindow();
	}

	private void initUI() {
		createEditorActions();
		setLayout(new BorderLayout());
		add(toolbar, BorderLayout.NORTH);
		wysEditor = createWysiwygEditor();
		add(new JScrollPane(wysEditor), BorderLayout.CENTER);

	}

	private void createEditorActions() {
		actionList = new ActionList("editor-actions");
		ActionList paraActions = new ActionList("paraActions");
		ActionList fontSizeActions = new ActionList("fontSizeActions");
		ActionList editActions = HTMLEditorActionFactory.createEditActionList();
		Action objectPropertiesAction = new HTMLElementPropertiesAction();
		//create editor popupmenus
		wysPopupMenu = ActionUIFactory.getInstance().createPopupMenu(editActions);
		wysPopupMenu.addSeparator();
		wysPopupMenu.add(objectPropertiesAction);
		// create edit menu   
		ActionList lst = new ActionList("edits");
		lst.addAll(editActions);
		lst.add(null);
		lst.add(new FindReplaceAction(false));
		actionList.addAll(lst);
		actionList.addAll(lst);
		fontSizeActions.addAll(lst);
		lst = HTMLEditorActionFactory.createInlineActionList();
		actionList.addAll(lst);
		Action act = new HTMLFontColorAction();
		actionList.add(act);
		act = new HTMLFontAction();
		actionList.add(act);
		act = new HTMLClearStylesAction();
		actionList.add(act);
		actionList.add(null);
		lst = HTMLEditorActionFactory.createBlockElementActionList();
		actionList.addAll(lst);
		paraActions.addAll(lst);
		lst = HTMLEditorActionFactory.createListElementActionList();
		actionList.addAll(lst);
		actionList.add(null);
		paraActions.addAll(lst);
		lst = HTMLEditorActionFactory.createAlignActionList();
		actionList.addAll(lst);
		lst = HTMLEditorActionFactory.createInsertTableElementActionList();
		actionList.addAll(lst);
		lst = HTMLEditorActionFactory.createDeleteTableElementActionList();
		actionList.addAll(lst);
		actionList.add(objectPropertiesAction);
		//create insert menu
		act = new HTMLLinkAction();
		actionList.add(act);
		act = new HTMLImageAction();
		actionList.add(act);
		act = new HTMLTableAction();
		actionList.add(act);
		act = new HTMLLineBreakAction();
		actionList.add(act);
		act = new HTMLHorizontalRuleAction();
		actionList.add(act);
		act = new HTMLUnicodeAction();
		actionList.add(act);
		createToolBar(paraActions, fontSizeActions);
	}

	@SuppressWarnings("unchecked")
	private void createToolBar(ActionList blockActs, ActionList fontSizeActs) {
		JToolBar tb1 = new JToolBar();
		tb1.setFloatable(false);
		PropertyChangeListener propLst = (PropertyChangeEvent evt) -> {
			if (evt.getPropertyName().equals("selected")) {
				if (evt.getNewValue().equals(Boolean.TRUE)) {
					paragraphCombo.removeActionListener(paragraphComboHandler);
					paragraphCombo.setSelectedItem(evt.getSource());
					paragraphCombo.addActionListener(paragraphComboHandler);
				}
			}
		};
		for (Iterator it = blockActs.iterator(); it.hasNext();) {
			Object o = it.next();
			if (o instanceof ActionDefault) {
				((ActionDefault) o).addPropertyChangeListener(propLst);
			}
		}
		tb1.add(new JLabel(i18n.strColon("style")));
		paragraphCombo = new JComboBox(toArray(blockActs));
		paragraphCombo.setToolTipText(i18n.str("paragraph"));
		paragraphCombo.addActionListener(paragraphComboHandler);
		paragraphCombo.setRenderer(new ParagraphComboRenderer());
		tb1.add(paragraphCombo);
		tb1.addSeparator();
		tb1.add(initButton(actionList, new HTMLHighlightAction()));		
		tb1.addSeparator();
		Log.write((reduced ? "reduced" : "extended")+" mode");
		if (!reduced) {
			tb1.add(new JLabel(i18n.strColon("font")));
			List<String> fonts = new ArrayList<>();
			fonts.add("Default");
			fonts.add("serif");
			fonts.add("sans-serif");
			fonts.add("monospaced");
			GraphicsEnvironment gEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
			fonts.addAll(Arrays.asList(gEnv.getAvailableFontFamilyNames()));
			fontFamilyCombo = new JComboBox(fonts.toArray());
			fontFamilyCombo.setToolTipText(i18n.str("font_desc"));
			fontFamilyCombo.addActionListener(fontChangeHandler);
			tb1.add(fontFamilyCombo);

			JButton fontSizeButton = new JButton(Images.getIcon("fontsize"));
			fontSizeButton.setToolTipText(i18n.str("fontsize_desc"));
			JPopupMenu sizePopup = ActionUIFactory.getInstance().createPopupMenu(fontSizeActs);
			ActionListener al = (ActionEvent e) -> {
				sizePopup.show(fontSizeButton, 0, fontSizeButton.getHeight());
			};
			fontSizeButton.addActionListener(al);
			tb1.add(fontSizeButton);

			Action act = new HTMLFontColorAction();
			actionList.add(act);
			addToToolBar(tb1, act);
			tb1.addSeparator();
		}

		JToolBar tb2 = new JToolBar();
		tb2.setFloatable(false);
		if (reduced) {
			tb2 = tb1;
		}
		//character styles
		tb2.add(initButton(actionList, new HTMLInlineAction(HTMLInlineAction.BOLD)));
		tb2.add(initButton(actionList, new HTMLInlineAction(HTMLInlineAction.ITALIC)));
		tb2.add(initButton(actionList, new HTMLInlineAction(HTMLInlineAction.UNDERLINE)));
		if (!reduced) {
			tb2.add(initButton(actionList, new HTMLInlineAction(HTMLInlineAction.STRIKE)));
			tb2.add(initButton(actionList, new HTMLInlineAction(HTMLInlineAction.SUB)));
			tb2.add(initButton(actionList, new HTMLInlineAction(HTMLInlineAction.SUP)));
		}
		tb2.addSeparator();
		//lists ol ul
		tb2.add(initButton(actionList, new HTMLBlockAction(HTMLBlockAction.OL)));
		tb2.add(initButton(actionList, new HTMLBlockAction(HTMLBlockAction.UL)));
		tb2.addSeparator();
		tb2.add(initButton(actionList, new HTMLAlignAction(HTMLAlignAction.LEFT)));
		tb2.add(initButton(actionList, new HTMLAlignAction(HTMLAlignAction.CENTER)));
		tb2.add(initButton(actionList, new HTMLAlignAction(HTMLAlignAction.RIGHT)));
		tb2.add(initButton(actionList, new HTMLAlignAction(HTMLAlignAction.JUSTIFY)));
		tb2.addSeparator();
		tb2.add(btEndnote=initButton(actionList, new HTMLEndnoteAction(editor, this)));
		btEndnote.setVisible(false);
		tb2.addSeparator();
		//links, image, table
		if (!reduced) {
			tb2.add(initButton(actionList, new HTMLLinkAction()));
			tb2.add(initButton(actionList, new HTMLImageAction()));
			tb2.add(initButton(actionList, new HTMLTableAction()));
		}
		//special characters (unicode, linebreak, dialog open/close)
		tb2.add(initButton(actionList, new HTMLLineBreakAction()));
		tb2.add(initButton(actionList, new HTMLUnicodeAction()));
		tb2.add(initButton(actionList, new HTMLCadratinAction(CADRATIN)));
		if (dlg_lang == 0 || dlg_lang == 1) {
			tb2.add(initButton(actionList, new HTMLCadratinAction(DIALOG_OPEN_EN)));
			tb2.add(initButton(actionList, new HTMLCadratinAction(DIALOG_CLOSE_EN)));
		}
		if (dlg_lang == 0 || dlg_lang == 2) {
			tb2.add(initButton(actionList, new HTMLCadratinAction(DIALOG_OPEN_FR)));
			tb2.add(initButton(actionList, new HTMLCadratinAction(DIALOG_CLOSE_FR)));
		}

		super.initToolbar();
		toolbar.setFloatable(false);
		toolbar.setFocusable(false);
		toolbar.setLayout(new BorderLayout());
		toolbar.add(tb1, BorderLayout.PAGE_START);
		if (!reduced) {
			toolbar.add(tb2, BorderLayout.PAGE_END);
		}
		JButton bt = new JButton(Images.getIcon("source"));
		bt.setName("source");
		bt.setText("");
		bt.setToolTipText(i18n.str("source"));
		bt.addActionListener(e -> {
			editor.changeTo("source");
		});
		tb1.add(bt);	
	}

	private void addToToolBar(JToolBar toolbar, Action act) {
		AbstractButton button = ActionUIFactory.getInstance().createButton(act);
		configToolbarButton(button);
		toolbar.add(button);
	}

	/**
	 * Converts an action list to an array. Any of the null "separators" or sub ActionLists are ommited from the array.
	 *
	 * @param lst
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Action[] toArray(ActionList lst) {
		List acts = new ArrayList();
		for (Iterator it = lst.iterator(); it.hasNext();) {
			Object v = it.next();
			if (v != null && v instanceof Action) {
				acts.add(v);
			}
		}
		return (Action[]) acts.toArray(new Action[acts.size()]);
	}

	private void configToolbarButton(AbstractButton button) {
		button.setText(null);
		button.setMnemonic(0);
		button.setMargin(new Insets(1, 1, 1, 1));
		button.setFocusable(false);
		button.setFocusPainted(false);
		Action a = button.getAction();
		if (a != null) {
			button.setToolTipText(a.getValue(Action.NAME).toString());
		}
	}

	private JEditorPane createWysiwygEditor() {
		JEditorPane ed = new JEditorPane();
		ed.setEditorKitForContentType("text/html", new WysiwygEditorKit());
		ed.setContentType("text/html");
		insertHTML(ed, "<p></p>", 0);
		ed.addCaretListener(caretHandler);
		ed.addFocusListener(focusHandler);
		ed.addMouseListener(popupHandler);
		HTMLDocument document = (HTMLDocument) ed.getDocument();
		CompoundUndoManager cuh = new CompoundUndoManager(document, new UndoManager());
		document.addUndoableEditListener(cuh);
		document.addDocumentListener(textChangedHandler);
		return ed;
	}

	/**
	 * inserts html into the wysiwyg editor
	 *
	 * @param editor
	 * @param html
	 * @param location
	 */
	private void insertHTML(JEditorPane editor, String html, int location) {
		try {
			HTMLEditorKit kit = (HTMLEditorKit) editor.getEditorKit();
			Document doc = editor.getDocument();
			StringReader reader = new StringReader(HTMLUtils.jEditorPaneizeHTML(html));
			kit.read(reader, doc, location);
		} catch (IOException | BadLocationException ex) {
			ex.printStackTrace(System.err);
		}
	}

	public void setText(String text) {
		String rtext = removeInvalidTags(text);
		wysEditor.setText("");
		insertHTML(wysEditor, rtext, 0);
		CompoundUndoManager.discardAllEdits(wysEditor.getDocument());
	}

	public String getText() {
		return removeInvalidTags(wysEditor.getText());
	}
	
	public boolean isTextChanged() {
		return this.isTextChanged;
	}

	/**
	 * removing invalid tags
	 *
	 * @param html
	 * @return
	 */
	private String removeInvalidTags(String html) {
		for (String tag : INVALID_TAGS) {
			html = deleteOccurance(html, "<" + tag + ">");
			html = deleteOccurance(html, "</" + tag + ">");
		}
		return html.trim();
	}

	/**
	 * delete a word from a String
	 *
	 * @param text
	 * @param word
	 * @return
	 */
	private String deleteOccurance(String text, String word) {
		StringBuilder sb = new StringBuilder(text);
		int p;
		while ((p = sb.toString().toLowerCase().indexOf(word.toLowerCase())) != -1) {
			sb.delete(p, p + word.length());
		}
		return sb.toString();
	}

	/**
	 * update the state
	 */
	private void updateState() {
		if (!reduced) {
			fontFamilyCombo.removeActionListener(fontChangeHandler);
			String fontName = HTMLUtils.getFontFamily(wysEditor);
			if (fontName == null) {
				fontFamilyCombo.setSelectedIndex(0);
			} else {
				fontFamilyCombo.setSelectedItem(fontName);
			}
			fontFamilyCombo.addActionListener(fontChangeHandler);
		}

		actionList.putContextValueForAll(HTMLTextEditAction.EDITOR, focusedEditor);
		actionList.updateEnabledForAll();
	}

	/**
	 * set the endnotes parameters
	 *
	 * @param sets
	 */
	public void endnoteSet(String sets) {
		//TODO Endnote set
		btEndnote.setVisible(true);
	}

	/**
	 * initialize endnotes
	 *
	 * @param init
	 */
	public void endnoteInit(String init) {
		//TODO Endnote hide
		btEndnote.setVisible(true);
	}

	/**
	 * get the endnotes table
	 *
	 * @return
	 */
	public String endnoteGet() {
		String str = "";
		return str;
	}

	public void endnoteHide() {
		//TODO Endnote hide
		btEndnote.setVisible(false);
	}

	/**
	 * handle listener for the caret
	 */
	private class CaretHandler implements CaretListener {

		@Override
		public void caretUpdate(CaretEvent e) {
			updateState();
		}
	}

	private class PopupHandler extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			checkForPopupTrigger(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			checkForPopupTrigger(e);
		}

		private void checkForPopupTrigger(MouseEvent e) {
			if (e.isPopupTrigger()) {
				JPopupMenu p;
				if (e.getSource() == wysEditor) {
					p = wysPopupMenu;
				} else {
					return;
				}
				p.show(e.getComponent(), e.getX(), e.getY());
			}
		}
	}

	private class FocusHandler implements FocusListener {

		@Override
		public void focusGained(FocusEvent e) {
			if (e.getComponent() instanceof JEditorPane) {
				JEditorPane ed = (JEditorPane) e.getComponent();
				CompoundUndoManager.updateUndo(ed.getDocument());
				focusedEditor = ed;
				updateState();
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			if (e.getComponent() instanceof JEditorPane) {
			}
		}
	}

	private class TextChangedHandler implements DocumentListener {

		@Override
		public void insertUpdate(DocumentEvent e) {
			textChanged();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			textChanged();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			textChanged();
		}

		private void textChanged() {
			isTextChanged = true;
		}
	}

	private class ParagraphComboHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == paragraphCombo) {
				Action a = (Action) (paragraphCombo.getSelectedItem());
				a.actionPerformed(e);
			}
		}
	}

	private class ParagraphComboRenderer extends DefaultListCellRenderer {

		@Override
		public Component getListCellRendererComponent(JList list, Object value, int index,
				boolean isSelected, boolean cellHasFocus) {
			if (value instanceof Action) {
				value = ((Action) value).getValue(Action.NAME);
			}
			return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		}
	}

	private class FontChangeHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == fontFamilyCombo && focusedEditor == wysEditor) {
				HTMLDocument document = (HTMLDocument) focusedEditor.getDocument();
				CompoundUndoManager.beginCompoundEdit(document);
				if (fontFamilyCombo.getSelectedIndex() != 0) {
					HTMLUtils.setFontFamily(wysEditor, fontFamilyCombo.getSelectedItem().toString());

				} else {
					HTMLUtils.setFontFamily(wysEditor, null);
				}
				CompoundUndoManager.endCompoundEdit(document);
			}
		}

		public void itemStateChanged(ItemEvent e) {

		}
	}
}

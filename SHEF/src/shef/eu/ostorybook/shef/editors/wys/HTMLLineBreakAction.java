/*
 * Created on Feb 25, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/**
 *
 */
public class HTMLLineBreakAction extends HTMLTextEditAction {

	public HTMLLineBreakAction() {
		super(i18n.str("line_break"));
		setMnemonic(i18n.mnem("line_break"));
		setSmallIcon(Images.getIcon("char_br"));
		setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.SHIFT_DOWN_MASK));
		setShortDescription(i18n.str("line_break"));
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		HTMLDocument document = (HTMLDocument) editor.getDocument();
		int pos = editor.getCaretPosition();
		String elName = document.getParagraphElement(pos).getName();
		HTML.Tag tag = HTML.getTag(elName);
		if (elName.toUpperCase().equals("P-IMPLIED")) {
			tag = HTML.Tag.IMPLIED;
		}
		HTMLEditorKit.InsertHTMLTextAction hta
				= new HTMLEditorKit.InsertHTMLTextAction("insertBR","<br>",tag,HTML.Tag.BR);
		hta.actionPerformed(e);
	}

}

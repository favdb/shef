/*
 * Copyright (C) oStorybook Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.ostorybook.shef.test;

import eu.ostorybook.shef.I18Nmsg;
import eu.ostorybook.shef.SHEFEditor;
import eu.ostorybook.shef.dialogs.EndnotesSetDlg;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author favdb
 */
public class SHEF implements ActionListener {

	private static final I18Nmsg i18n = I18Nmsg.getInstance();
	private JFrame mainFrame;
	private SHEFEditor editor;
	private String fileName = "";
	private boolean dev = false;
	private int dlg_lang;
	private boolean mode;

	public SHEF() {
	}

	public void init() {
		String texte="";
		InputStream in = SHEF.class.getResourceAsStream("htmlsnip.txt");
		try {
			texte=read(in);
		} catch (IOException ex) {
			ex.printStackTrace(System.err);
		}
		editor = new SHEFEditor(dlg_lang, mode);
		editor.setText(texte);
		editor.setCaretPosition(0);
		mainFrame = new JFrame();
		//mainFrame.setLayout(new BorderLayout());
		mainFrame.setName("mainFrame");
		JMenuBar menubar = initMenu();
		mainFrame.setJMenuBar(menubar);
		mainFrame.setTitle("HTML Editor Demo");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(800, 600);
		mainFrame.setVisible(true);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.add(editor);
	}

	private JMenuBar initMenu() {
		JMenuBar menu = new JMenuBar();
		JMenu fichier = new JMenu(i18n.str("file"));
		fichier.add(initMenuItem("file.new"));
		fichier.add(initMenuItem("file.open"));
		fichier.add(initMenuItem("file.save"));
		fichier.add(initMenuItem("file.saveas"));
		fichier.add(initMenuItem("file.exit"));
		menu.add(fichier);
		JMenu param=new JMenu(i18n.str("options"));
		param.add(initMenuItem("endnotes.set"));
		JMenu paramMode=new JMenu(i18n.str("mode"));
		paramMode.add(initMenuItem("mode.reduced"));
		paramMode.add(initMenuItem("mode.extended"));
		param.add(paramMode);
		JMenu paramLang=new JMenu(i18n.str("lang"));
		paramLang.add(initMenuItem("lang.en"));
		paramLang.add(initMenuItem("lang.fr"));
		paramLang.add(initMenuItem("lang.all"));
		param.add(paramLang);
		menu.add(param);
		return (menu);
	}

	private JMenuItem initMenuItem(String name) {
		JMenuItem item = new JMenuItem(i18n.str(name));
		item.setName(name);
		item.addActionListener(this);
		return item;
	}

	public static void main(String args[]) {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | IllegalAccessException
				| InstantiationException | UnsupportedLookAndFeelException e) {
		}

		SwingUtilities.invokeLater(() -> {
			SHEF demo = new SHEF();
			demo.init();
		});
	}

	public String read(InputStream input) throws IOException {
		InputStreamReader stream = new InputStreamReader(input);
		StringBuilder sb;
		try (BufferedReader reader = new BufferedReader(stream)) {
			sb = new StringBuilder();
			int ch;
			while ((ch = reader.read()) != -1) {
				sb.append((char) ch);
			}
		}
		return sb.toString();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		if (src instanceof JMenuItem) {
			switch (((JMenuItem) src).getName()) {
				case "file.exit":
					System.exit(0);
					return;
				case "file.new":
					fileNew();
					return;
				case "file.open":
					fileOpen();
					return;
				case "file.save":
					fileSave();
					return;
				case "file.saveas":
					fileSaveAs();
					return;
				case "endnotes.set":
					JOptionPane.showMessageDialog(mainFrame,
							"not implemented yet",
							"Attention",
							JOptionPane.INFORMATION_MESSAGE);
					if (dev) {
						EndnotesSetDlg.show(mainFrame);
					}
					break;
				case "mode.reduced":
					System.out.println("set reduced mode");
					mode=true;
					break;
				case "mode.extended":
					System.out.println("set extended mode");
					mode=false;
					break;
				case "lang.en":
					System.out.println("set en lang");
					dlg_lang=1;
					break;
				case "lang.fr":
					System.out.println("set fr lang");
					dlg_lang=2;
					break;
				case "lang.all":
					System.out.println("set all lang");
					dlg_lang=0;
					break;
			}
			String texte=editor.getText();
			mainFrame.remove(editor);
			editor=new SHEFEditor(dlg_lang, mode);
			editor.setText(texte);
			editor.setCaretPosition(0);
			mainFrame.add(editor);
		}
	}

	private void fileNew() {
		fileName = "";
		editor.setText("");
		editor.setCaretPosition(0);
	}

	private void fileOpen() {
		JFileChooser fc = new JFileChooser();
		fc.setSelectedFile(new File(fileName));
		fc.setApproveButtonText(i18n.str("file.open"));
		if (fc.showOpenDialog(mainFrame) == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			if (!file.exists() || !file.isFile()) {
				return;
			}
			try {
				try (InputStream in = new FileInputStream(file)) {
					editor.setText(read(in));
					fileName = file.getAbsolutePath();
				}
			} catch (FileNotFoundException ex) {
				ex.printStackTrace(System.err);
			} catch (IOException ex) {
				ex.printStackTrace(System.err);
			}
		}
	}

	private void fileSave() {
		File file = new File(fileName);
		if (fileName.isEmpty()) {
			JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fc.setSelectedFile(new File(fileName));
			fc.setApproveButtonText(i18n.str("file.save"));
			if (fc.showOpenDialog(mainFrame) == JFileChooser.APPROVE_OPTION) {
				file = fc.getSelectedFile();
				if (!file.exists() || !file.isFile()) {
					return;
				}
			}
		}
		try {
			try (OutputStream out = new FileOutputStream(file)) {
				out.write(editor.getText().getBytes());
				fileName = file.getAbsolutePath();
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace(System.err);
		} catch (IOException ex) {
			ex.printStackTrace(System.err);
		}
	}

	private void fileSaveAs() {
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fc.setSelectedFile(new File(fileName));
		fc.setApproveButtonText(i18n.str("file.saveas"));
		if (fc.showOpenDialog(mainFrame) == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			if (file.exists() || !file.isFile()) {
				return;
			}
			try {
				try (OutputStream out = new FileOutputStream(file)) {
					out.write(editor.getText().getBytes());
					fileName = file.getAbsolutePath();
				}
			} catch (FileNotFoundException ex) {
				ex.printStackTrace(System.err);
			} catch (IOException ex) {
				ex.printStackTrace(System.err);
			}
		}
	}

}

/*
 * Created on Nov 2, 2007
 */
package eu.ostorybook.shef.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;

/**
 * @author Bob Tantlinger
 *
 */
public class EditCutAction extends BasicEditAction {

	public EditCutAction() {
		super("cut");
		putValue(Action.NAME, i18n.str("cut"));
		setSmallIcon("edit_cut");
		setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK));
		setMnemonic("cut");
		addShouldBeEnabledDelegate((Action a) -> {
			JEditorPane ed = getCurrentEditor();
			return ed != null && ed.getSelectionStart() != ed.getSelectionEnd();
		});
		putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
	}

	@Override
	protected void doEdit(ActionEvent e, JEditorPane editor) {
		editor.cut();
	}

	@Override
	protected void contextChanged() {
		super.contextChanged();
		this.updateEnabledState();
	}

}

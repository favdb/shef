/*
 * Copyright (C) 2020 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.ostorybook.shef.actions;

/**
 *
 * @author favdb
 */
public class Endnote {
	private int number=-1;
	private String text="";

	public Endnote(String text) {
		this.text=text;
	}

	public Endnote(int number, String text) {
		this.number=number;
		this.text=text;
	}

	String getText() {
		return(text);
	}

	void setText(String text) {
		this.text=text;
	}

	void setText(int number, String text) {
		this.number=number;
		this.text=text;
	}

	public static String getLinkToEndnote(String path) {
		String link="";
		return(link);
	}
	
}

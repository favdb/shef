/*
 * Created on Nov 2, 2007
 */
package eu.ostorybook.shef.actions;

import eu.ostorybook.shef.editors.wys.HTMLTextEditAction;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;

/**
 * Action suitable for when wysiwyg or source context does not matter.
 *
 * @author Bob Tantlinger
 *
 */
public abstract class BasicEditAction extends HTMLTextEditAction {

	/**
	 * @param name
	 */
	public BasicEditAction(String name) {
		super(name);
	}

	@Override
	protected final void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		doEdit(e, editor);
	}

	protected abstract void doEdit(ActionEvent e, JEditorPane editor);

	protected void updateContextState(JEditorPane editor) {

	}

	@Override
	protected final void updateWysiwygContextState(JEditorPane wysEditor) {
		updateContextState(wysEditor);
	}

}

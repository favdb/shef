package eu.ostorybook.shef.actions.manager;

import javax.swing.Icon;

public interface IconResolver {

	Icon resolveIcon(String string);
}

package eu.ostorybook.shef.actions.manager;


public interface DelegatesEnabled {
  void addShouldBeEnabledDelegate(ShouldBeEnabledDelegate param);
  
  void removeShouldBeEnabledDelegate(ShouldBeEnabledDelegate param);
}

package eu.ostorybook.shef.actions.manager;

import java.awt.event.ItemListener;

public interface ItemListenerDelegator {

	void setItemListenerDelegate(ItemListener param);
}

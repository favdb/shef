/*
 * Copyright (C) 2020 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.ostorybook.shef.tools;

import eu.ostorybook.shef.actions.manager.ActionBasic;
import eu.ostorybook.shef.actions.manager.ActionList;
import eu.ostorybook.shef.actions.manager.Separator;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToggleButton;

/**
 *
 * @author favdb
 */
public class SwingUtil {
	
	public static JMenu createMenu(ActionList actions) {
		JMenu menu=new JMenu();
		int c=0;
		for (Object ac:actions) {
			//first action is the JMenu, others are JMenuItem
			if (c==0) {
				ActionBasic act=(ActionBasic)ac;
				if (act==null) continue;
				menu.setText(act.getActionName());
				if (act.getMnemonic()>0) {
					menu.setMnemonic(act.getMnemonic());
				}
				if (act.getMenuShowsIcon() && act.getSmallIcon()!=null) {
					menu.setIcon(act.getSmallIcon());
				}
				c++;
				continue;
			}
			//other actions
			if (ac instanceof Separator) {
				menu.addSeparator();
			} else if (ac instanceof ActionList) {
				menu.add(createMenu((ActionList)ac));
			} else if (ac instanceof Action) {
				menu.add(createMenuItem((Action)ac));
			}
		}
		return(menu);
	}
	
	public static JMenuItem createMenuItem(Action action) {
		JMenuItem item=new JMenuItem();
		ActionBasic act=(ActionBasic)action;
		item.setText(act.getActionName());
		if (act.getMnemonic()>0) {
			item.setMnemonic(act.getMnemonic());
		}
		if (act.getMenuShowsIcon()) {
			item.setIcon(act.getSmallIcon());
		}
		return(item);
	}
	
	public static JPopupMenu createPopupMenu(ActionList actions) {
		JPopupMenu popup=new JPopupMenu();
		for (Object element : actions) {
			if (element == null || element instanceof Separator) {
				popup.addSeparator();
			} else if (element instanceof ActionList) {
				popup.add(createMenu((ActionList) element));
			} else if (element instanceof Action) {
				popup.add(createMenuItem((Action) element));
			}
		}
		return(popup);
	}
	
	public AbstractButton createButton(ActionBasic action) {
		ActionBasic.BUTTON_TYPE buttonType = action.getButtonType();
		AbstractButton button;
		switch(buttonType) {
			case TOGGLE:
				button=new JToggleButton(action);
				break;
			case RADIO:
				button=new JRadioButtonMenuItem(action);
				break;
			case CHECKBOX:
				button=new JCheckBoxMenuItem(action);
				break;
			case NORMAL:
			default:
				button=new JButton(action);
				break;
		}
		//never show any text
		button.setText("");
		button.setIcon(action.getSmallIcon());
		button.setToolTipText(action.getShortDescription());
		return button;
	}

}

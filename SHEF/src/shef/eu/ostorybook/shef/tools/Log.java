/**
 * from novawork API
 */
package eu.ostorybook.shef.tools;

public final class Log {

	private static int LEVEL = 4;
	public static final int
			NONE = 0,
			DEVEL = 1,
			DEBUG = 2,
			CONFIG = 3,
			ERROR = 4;

	public static void setLevel(int level) {
		LEVEL = level;
	}

	public static void write(int priority, Throwable t) {
		if (priority == ERROR) {
			t.printStackTrace(System.err);
		} else if (LEVEL < priority) {
			t.printStackTrace(System.out);
		}
	}

	public static void write(String text) {
		write(2, text);
	}

	public static void write(int priority, String text) {
		if (priority == ERROR) {
			System.err.println(text);
		} else if (LEVEL < priority) {
			System.out.println(text);
		}
	}
}

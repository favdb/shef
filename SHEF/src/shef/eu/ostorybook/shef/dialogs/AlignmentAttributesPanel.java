/*
 * Created on Dec 21, 2005
 *
 */
package eu.ostorybook.shef.dialogs;

import java.awt.GridBagLayout;
import javax.swing.JCheckBox;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.util.*;
import javax.swing.BorderFactory;

/**
 * A panel for editing table alignment attributes
 *
 * @author Bob Tantlinger
 *
 */
public class AlignmentAttributesPanel extends HTMLAttributeEditorPanel {

	private static final String VERT_ALIGNMENTS[] = {"top", "middle", "bottom"};
	private static final String HORIZ_ALIGNMENTS[] = {"left", "center", "right", "justify"};
	private JCheckBox vAlignCB = null;
	private JCheckBox hAlignCB = null;
	private JComboBox vLocCombo = null;
	private JComboBox hLocCombo = null;

	public AlignmentAttributesPanel() {
		this(new Hashtable());
	}

	public AlignmentAttributesPanel(Hashtable attr) {
		super(attr);
		initialize();
		updateComponentsFromAttribs();
	}

	@Override
	public void updateComponentsFromAttribs() {
		if (attribs.containsKey("align")) {
			hAlignCB.setSelected(true);
			hLocCombo.setEnabled(true);
			hLocCombo.setSelectedItem(attribs.get("align"));
		} else {
			hAlignCB.setSelected(false);
			hLocCombo.setEnabled(false);
		}

		if (attribs.containsKey("valign")) {
			vAlignCB.setSelected(true);
			vLocCombo.setEnabled(true);
			vLocCombo.setSelectedItem(attribs.get("valign"));
		} else {
			vAlignCB.setSelected(false);
			vLocCombo.setEnabled(false);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void updateAttribsFromComponents() {
		if (vAlignCB.isSelected()) {
			attribs.put("valign", vLocCombo.getSelectedItem().toString());
		} else {
			attribs.remove("valign");
		}
		if (hAlignCB.isSelected()) {
			attribs.put("align", hLocCombo.getSelectedItem().toString());
		} else {
			attribs.remove("align");
		}
	}

	public void setComponentStates(Hashtable attribs) {
		if (attribs.containsKey("align")) {
			hAlignCB.setSelected(true);
			hLocCombo.setEnabled(true);
			hLocCombo.setSelectedItem(attribs.get("align"));
		} else {
			hAlignCB.setSelected(false);
			hLocCombo.setEnabled(false);
		}

		if (attribs.containsKey("valign")) {
			vAlignCB.setSelected(true);
			vLocCombo.setEnabled(true);
			vLocCombo.setSelectedItem(attribs.get("valign"));
		} else {
			vAlignCB.setSelected(false);
			vLocCombo.setEnabled(false);
		}
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		setLayout(new GridBagLayout());
		setSize(185, 95);
		setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(i18n.str("content_alignment")),
				BorderFactory.createEmptyBorder(2, 5, 2, 5)));
		setPreferredSize(new java.awt.Dimension(185, 95));
		setMaximumSize(getPreferredSize());
		setMinimumSize(getPreferredSize());
		
		add(getVAlignCB(), new GBC("0,0,fill WEST, insets 0 0 5 5"));
		add(getHAlignCB(), new GBC("1,0,fill WEST, insets 0 0 0 5"));
		add(getVLocCombo(), new GBC("0,1,fill NONE, weightx 1.0,anchor WEST, insets 0 0 5 0"));
		add(getHLocCombo(), new GBC("1,1,fill NONE, weightx 1.0, anchor WEST"));
	}

	/**
	 * This method initializes vAlignCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getVAlignCB() {
		if (vAlignCB == null) {
			vAlignCB = new JCheckBox();
			vAlignCB.setText(i18n.str("vertical"));
			vAlignCB.addActionListener((ActionEvent e) -> {
				vLocCombo.setEnabled(vAlignCB.isSelected());
			});
		}
		return vAlignCB;
	}

	/**
	 * This method initializes hAlignCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getHAlignCB() {
		if (hAlignCB == null) {
			hAlignCB = new JCheckBox();
			hAlignCB.setText(i18n.str("horizontal"));
			hAlignCB.addActionListener((ActionEvent e) -> {
				hLocCombo.setEnabled(hAlignCB.isSelected());
			});
		}
		return hAlignCB;
	}

	/**
	 * This method initializes vLocCombo
	 *
	 * @return javax.swing.JComboBox
	 */
	@SuppressWarnings("unchecked")
	private JComboBox getVLocCombo() {
		if (vLocCombo == null) {
			vLocCombo = new JComboBox(VERT_ALIGNMENTS);
		}
		return vLocCombo;
	}

	/**
	 * This method initializes hLocCombo
	 *
	 * @return javax.swing.JComboBox
	 */
	@SuppressWarnings("unchecked")
	private JComboBox getHLocCombo() {
		if (hLocCombo == null) {
			hLocCombo = new JComboBox(HORIZ_ALIGNMENTS);
		}
		return hLocCombo;
	}

}

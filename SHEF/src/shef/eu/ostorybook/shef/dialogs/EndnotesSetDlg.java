/*
 * Copyright (C) 2020 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.ostorybook.shef.dialogs;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.Frame;
import javax.swing.Icon;

/**
 *
 * @author favdb
 */
public class EndnotesSetDlg extends OptionDialog {
	public static boolean show(Frame parent) {
		EndnotesSetDlg dlg=new EndnotesSetDlg(parent);
		dlg.setVisible(true);
		return dlg.hasUserCancelled();
	}
	
	private boolean okData=false;

	public EndnotesSetDlg(Frame parent, String headerTitle, String desc, Icon icon) {
		super(parent, headerTitle, desc, icon);
		initAll();
	}

	public EndnotesSetDlg(Frame mainFrame) {
		this(mainFrame, i18n.str("endnotes.set"), i18n.str("endnotes.set"), Images.getIcon("char_endnote"));
	}

	private void initAll() {
		init();
		initUi();
	}

	private void init() {
	}

	private void initUi() {
		//TODO Endnote à compléter
	}

	@Override
	protected boolean isValidData() {
		//TODO Endnote à compléter
		return true;
	}
	
	public boolean isOK() {
		return okData;
	}

}

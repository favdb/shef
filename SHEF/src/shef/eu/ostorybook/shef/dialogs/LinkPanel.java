/*
 * Created on Jan 13, 2006
 *
 */
package eu.ostorybook.shef.dialogs;

import eu.ostorybook.shef.actions.TextEditPopupManager;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.util.Hashtable;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LinkPanel extends HTMLAttributeEditorPanel {

	private JPanel hlinkPanel = null;
	private JLabel urlLabel = null;
	private JLabel textLabel = null;
	private JTextField urlField = null;
	private JTextField textField = null;
	private HTMLAttributeEditorPanel linkAttrPanel = null;
	private boolean urlFieldEnabled;

	/**
	 * This is the default constructor
	 */
	public LinkPanel() {
		this(true);
	}

	/**
	 * @param attr
	 */
	public LinkPanel(boolean urlFieldEnabled) {
		this(new Hashtable(), urlFieldEnabled);
	}

	public LinkPanel(Hashtable attr, boolean urlFieldEnabled) {
		super();
		this.urlFieldEnabled = urlFieldEnabled;
		initialize();
		setAttributes(attr);
		updateComponentsFromAttribs();
	}

	@Override
	public void updateComponentsFromAttribs() {
		linkAttrPanel.updateComponentsFromAttribs();
		if (attribs.containsKey("href")) {
			urlField.setText(attribs.get("href").toString());
		} else {
			urlField.setText("");
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void updateAttribsFromComponents() {
		linkAttrPanel.updateAttribsFromComponents();
		attribs.put("href", urlField.getText());
	}

	@Override
	public void setAttributes(Map at) {
		super.setAttributes(at);
		linkAttrPanel.setAttributes(attribs);
	}

	public void setLinkText(String text) {
		textField.setText(text);
	}

	public String getLinkText() {
		return textField.getText();
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		this.setLayout(new BorderLayout(5, 5));
		this.setSize(328, 218);
		this.add(getHlinkPanel(), BorderLayout.NORTH);
		this.add(getLinkAttrPanel(), BorderLayout.CENTER);

		TextEditPopupManager popupMan = TextEditPopupManager.getInstance();//new TextEditPopupManager();
		popupMan.registerJTextComponent(urlField);
		popupMan.registerJTextComponent(textField);
	}

	/**
	 * This method initializes hlinkPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getHlinkPanel() {
		if (hlinkPanel == null) {
			textLabel = new JLabel(i18n.str("text"));
			urlLabel = new JLabel(i18n.str("url"));
			hlinkPanel = new JPanel();
			hlinkPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createTitledBorder(null, i18n.str("link"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null), javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5)));
			hlinkPanel.setLayout(new GridBagLayout());
			hlinkPanel.add(urlLabel, new GBC("0,0,anchor WEST, insets 0 0 0 5"));
			hlinkPanel.add(textLabel, new GBC("1,0,anchor WEST, insets 0 0 0 5"));
			hlinkPanel.add(getUrlField(), new GBC("0,1,fill HORIZONTAL, weightx 1.0, insets 0 0 5 0"));
			hlinkPanel.add(getTextField(), new GBC("1,1,fill HORIZONTAL,weightx 1.0"));
		}
		return hlinkPanel;
	}

	/**
	 * This method initializes urlField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getUrlField() {
		if (urlField == null) {
			urlField = new JTextField();
			urlField.setEditable(urlFieldEnabled);
			//urlField.setEditable(true);
		}
		return urlField;
	}

	/**
	 * This method initializes textField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
		}
		return textField;
	}

	private JPanel getLinkAttrPanel() {
		if (linkAttrPanel == null) {
			linkAttrPanel = new LinkAttributesPanel();
		}

		return linkAttrPanel;
	}

}

/*
 * Created on Dec 21, 2005
 *
 */
package eu.ostorybook.shef.dialogs;

import java.awt.GridBagLayout;
import java.util.Hashtable;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * Panel for editing the size of a table cell
 *
 * @author Bob Tantlinger
 *
 */
public class SizeAttributesPanel extends HTMLAttributeEditorPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private static final String MEASUREMENTS[] = {"percent", "pixels"};     //$NON-NLS-2$

	private JCheckBox widthCB = null;
	private JCheckBox heightCB = null;
	private JSpinner widthField = null;
	private JSpinner heightField = null;
	private JComboBox wMeasurementCombo = null;
	private JComboBox hMeasurementCombo = null;

	public SizeAttributesPanel() {
		this(new Hashtable());
	}

	public SizeAttributesPanel(Hashtable attr) {
		super(attr);
		initialize();
		updateComponentsFromAttribs();
	}

	@Override
	public void updateComponentsFromAttribs() {
		if (attribs.containsKey("width")) {
			widthCB.setSelected(true);
			String w = attribs.get("width").toString();
			if (w.endsWith("%")) {
				w = w.substring(0, w.length() - 1);
			} else {
				wMeasurementCombo.setSelectedIndex(1);
			}
			try {
				widthField.getModel().setValue(w);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
			wMeasurementCombo.setEnabled(true);
			widthField.setEnabled(true);
		} else {
			widthCB.setSelected(false);
			widthField.setEnabled(false);
			wMeasurementCombo.setEnabled(false);
		}

		if (attribs.containsKey("height")) {
			heightCB.setSelected(true);
			String h = attribs.get("height").toString();
			if (h.endsWith("%")) {
				h = h.substring(0, h.length() - 1);
			} else {
				hMeasurementCombo.setSelectedIndex(1);
			}
			try {
				heightField.getModel().setValue(h);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
			hMeasurementCombo.setEnabled(true);
			heightField.setEnabled(true);
		} else {
			heightCB.setSelected(false);
			heightField.setEnabled(false);
			hMeasurementCombo.setEnabled(false);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void updateAttribsFromComponents() {
		if (widthCB.isSelected()) {
			String w = widthField.getModel().getValue().toString();
			if (wMeasurementCombo.getSelectedIndex() == 0) {
				w += "%";
			}
			attribs.put("width", w);
		} else {
			attribs.remove("width");
		}

		if (heightCB.isSelected()) {
			String h = heightField.getModel().getValue().toString();
			if (hMeasurementCombo.getSelectedIndex() == 0) {
				h += "%";
			}
			attribs.put("height", h);
		} else {
			attribs.remove("height");
		}
	}

	public void setComponentStates(Hashtable attribs) {
		if (attribs.containsKey("width")) {
			widthCB.setSelected(true);
			String w = attribs.get("width").toString();
			if (w.endsWith("%")) {
				w = w.substring(0, w.length() - 1);
			} else {
				wMeasurementCombo.setSelectedIndex(1);
			}
			try {
				widthField.getModel().setValue(w);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
			wMeasurementCombo.setEnabled(true);
			widthField.setEnabled(true);
		} else {
			widthCB.setSelected(false);
			widthField.setEnabled(false);
			wMeasurementCombo.setEnabled(false);
		}

		if (attribs.containsKey("height")) {
			heightCB.setSelected(true);
			String h = attribs.get("height").toString();
			if (h.endsWith("%")) {
				h = h.substring(0, h.length() - 1);
			} else {
				hMeasurementCombo.setSelectedIndex(1);
			}
			try {
				heightField.getModel().setValue(h);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
			hMeasurementCombo.setEnabled(true);
			heightField.setEnabled(true);
		} else {
			heightCB.setSelected(false);
			heightField.setEnabled(false);
			hMeasurementCombo.setEnabled(false);
		}
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		setLayout(new GridBagLayout());
		setSize(215, 95);
		setPreferredSize(new java.awt.Dimension(215, 95));
		setMaximumSize(getPreferredSize());
		setMinimumSize(getPreferredSize());
		setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createTitledBorder(null, i18n.str("size"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null), javax.swing.BorderFactory.createEmptyBorder(2, 5, 2, 5)));

		add(getWidthCB(), new GBC("0,0,anchor W, ins 0 0 5 5"));
		add(getHeightCB(), new GBC("1,0,anchor W, ins 0 0 0 5"));
		add(getWidthField(), new GBC("0,1,fill N, wx 0.0, anchor W, ins 0 0 0 5, ipadx 0"));
		add(getHeightField(), new GBC("1,1,fill N, wx 0.0, anchor W, ins 0 0 0 5,ipadx 0"));
		add(getWMeasurementCombo(), new GBC("0,2,fill N, wx 1.0, anchor W, ins 0 0 5 0"));
		add(getHMeasurementCombo(), new GBC("1,2,fill N, wx 0.0, anchor W, ipadx 0"));
	}

	/**
	 * This method initializes widthCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getWidthCB() {
		if (widthCB == null) {
			widthCB = new JCheckBox();
			widthCB.setText(i18n.str("width"));
			widthCB.addItemListener((java.awt.event.ItemEvent e) -> {
				widthField.setEnabled(widthCB.isSelected());
				wMeasurementCombo.setEnabled(widthCB.isSelected());
			});
		}
		return widthCB;
	}

	/**
	 * This method initializes heightCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getHeightCB() {
		if (heightCB == null) {
			heightCB = new JCheckBox();
			heightCB.setText(i18n.str("height"));
			heightCB.addItemListener((java.awt.event.ItemEvent e) -> {
				heightField.setEnabled(heightCB.isSelected());
				hMeasurementCombo.setEnabled(heightCB.isSelected());
			});
		}
		return heightCB;
	}

	/**
	 * This method initializes widthField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getWidthField() {
		if (widthField == null) {
			widthField = new JSpinner(new SpinnerNumberModel(1, 1, 999, 1));
		}
		return widthField;
	}

	/**
	 * This method initializes heightField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getHeightField() {
		if (heightField == null) {
			heightField = new JSpinner(new SpinnerNumberModel(1, 1, 999, 1));
		}
		return heightField;
	}

	/**
	 * This method initializes wMeasurementCombo
	 *
	 * @return javax.swing.JComboBox
	 */
	@SuppressWarnings("unchecked")
	private JComboBox getWMeasurementCombo() {
		if (wMeasurementCombo == null) {
			wMeasurementCombo = new JComboBox(MEASUREMENTS);
		}
		return wMeasurementCombo;
	}

	/**
	 * This method initializes hMeasurementCombo
	 *
	 * @return javax.swing.JComboBox
	 */
	@SuppressWarnings("unchecked")
	private JComboBox getHMeasurementCombo() {
		if (hMeasurementCombo == null) {
			hMeasurementCombo = new JComboBox(MEASUREMENTS);
		}
		return hMeasurementCombo;
	}

}

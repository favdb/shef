/*
 * Created on Jan 17, 2006
 *
 */
package eu.ostorybook.shef.dialogs;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.Dialog;
import java.awt.Frame;
import java.util.Map;
import javax.swing.Icon;

public class ElementStyleDialog extends OptionDialog {

	private static Icon icon = Images.getIcon("pencil");
	private static String title = i18n.str("element_style");
	private static String desc = i18n.str("element_style_desc");
	private StyleAttributesPanel stylePanel;

	public ElementStyleDialog(Frame parent) {
		super(parent, title, desc, icon);
		init();
	}

	public ElementStyleDialog(Dialog parent) {
		super(parent, title, desc, icon);
		init();
	}

	private void init() {
		stylePanel = new StyleAttributesPanel();
		setContentPane(stylePanel);
		pack();
		setSize(300, getHeight());
		setResizable(false);
	}

	public void setStyleAttributes(Map attr) {
		stylePanel.setAttributes(attr);
	}

	public Map getStyleAttributes() {
		return stylePanel.getAttributes();
	}

}

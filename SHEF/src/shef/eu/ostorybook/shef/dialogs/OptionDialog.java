/*
 * Created on Jan 10, 2006
 *
 */
package eu.ostorybook.shef.dialogs;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import javax.swing.Icon;
import javax.swing.JPanel;

public class OptionDialog extends StandardDialog {

	private JPanel internalContentPane;
	private Container contentPane;

	public OptionDialog(Frame parent, String headerTitle, String desc, Icon icon) {
		super(parent, headerTitle, BUTTONS_RIGHT);
		init(headerTitle, desc, icon);
	}

	public OptionDialog(Dialog parent, String headerTitle, String desc, Icon icon) {
		super(parent, headerTitle, BUTTONS_RIGHT);
		init(headerTitle, desc, icon);
	}

	private void init(String title, String desc, Icon icon) {
		internalContentPane = new JPanel(new BorderLayout());
		HeaderPanel hp = new HeaderPanel();
		hp.setTitle(title);
		hp.setDescription(desc);
		hp.setIcon(icon);
		internalContentPane.add(hp, BorderLayout.NORTH);
		super.setContentPane(internalContentPane);
	}

	@Override
	public Container getContentPane() {
		return contentPane;
	}

	@Override
	public void setContentPane(Container c) {
		contentPane = c;
		internalContentPane.add(c, BorderLayout.CENTER);
	}

}

/*
 * Created on Jan 18, 2006
 *
 */
package eu.ostorybook.shef.dialogs;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.Dialog;
import java.awt.Frame;
import java.util.*;
import javax.swing.Icon;

public class ListDialog extends OptionDialog {

	public static final int UNORDERED = ListAttributesPanel.UL_LIST;
	public static final int ORDERED = ListAttributesPanel.OL_LIST;
	private static Icon icon = Images.getIcon("categories");
	private static String title = i18n.str("list_properties");
	private static String desc = i18n.str("list_properties_desc");
	private ListAttributesPanel listAttrPanel;

	public ListDialog(Frame parent) {
		super(parent, title, desc, icon);
		init();
	}

	public ListDialog(Dialog parent) {
		super(parent, title, desc, icon);
		init();
	}

	private void init() {
		listAttrPanel = new ListAttributesPanel();
		setContentPane(listAttrPanel);
		pack();
		setSize(220, getHeight());
		setResizable(false);
	}

	public void setListType(int t) {
		listAttrPanel.setListType(t);
	}

	public int getListType() {
		return listAttrPanel.getListType();
	}

	public void setListAttributes(Map attr) {
		listAttrPanel.setAttributes(attr);
	}

	public Map getListAttributes() {
		return listAttrPanel.getAttributes();
	}
}

/*
 * Created on Dec 21, 2005
 *
 */
package eu.ostorybook.shef.dialogs;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.Dialog;
import java.awt.Frame;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class TablePropertiesDialog extends OptionDialog {

	private static Icon icon = Images.getIcon("table");
	private static String title = i18n.str("table_properties");
	private static String desc = i18n.str("table_properties_desc");
	private TableAttributesPanel tableProps = new TableAttributesPanel();
	private RowAttributesPanel rowProps = new RowAttributesPanel();
	private CellAttributesPanel cellProps = new CellAttributesPanel();

	public TablePropertiesDialog(Frame parent) {
		super(parent, title, desc, icon);
		init();
	}

	public TablePropertiesDialog(Dialog parent) {
		super(parent, title, desc, icon);
		init();
	}

	private void init() {
		Border emptyBorder = new EmptyBorder(5, 5, 5, 5);
		Border titleBorder = BorderFactory.createTitledBorder(i18n.str("table_properties"));
		tableProps.setBorder(BorderFactory.createCompoundBorder(emptyBorder, titleBorder));
		rowProps.setBorder(emptyBorder);
		cellProps.setBorder(emptyBorder);
		JTabbedPane tabs = new JTabbedPane();
		tabs.add(tableProps, i18n.str("table"));
		tabs.add(rowProps, i18n.str("row"));
		tabs.add(cellProps, i18n.str("cell"));
		setContentPane(tabs);
		setSize(440, 375);
		setResizable(false);
	}

	public void setTableAttributes(Map at) {
		tableProps.setAttributes(at);
	}

	public void setRowAttributes(Map at) {
		rowProps.setAttributes(at);
	}

	public void setCellAttributes(Map at) {
		cellProps.setAttributes(at);
	}

	public Map getTableAttributes() {
		return tableProps.getAttributes();
	}

	public Map getRowAttribures() {
		return rowProps.getAttributes();
	}

	public Map getCellAttributes() {
		return cellProps.getAttributes();
	}

}

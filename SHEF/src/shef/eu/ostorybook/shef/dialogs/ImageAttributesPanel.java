/*
 * Created on Jan 14, 2006
 *
 */
package eu.ostorybook.shef.dialogs;

import eu.ostorybook.shef.actions.TextEditPopupManager;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

public class ImageAttributesPanel extends HTMLAttributeEditorPanel {

	private static final String ALIGNMENTS[] = {"top", "middle", "bottom", "left", "right"};

	private JLabel imgUrlLabel = null;
	private JCheckBox altTextCB = null;
	private JCheckBox widthCB = null;
	private JCheckBox heightCB = null;
	private JCheckBox borderCB = null;
	private JSpinner widthField = null;
	private JSpinner heightField = null;
	private JSpinner borderField = null;
	private JCheckBox vSpaceCB = null;
	private JCheckBox hSpaceCB = null;
	private JCheckBox alignCB = null;
	private JSpinner vSpaceField = null;
	private JSpinner hSpaceField = null;
	private JComboBox alignCombo = null;
	private JTextField imgUrlField = null;
	private JTextField altTextField = null;
	private JPanel attribPanel = null;

	private JPanel spacerPanel = null;

	/**
	 * This is the default constructor
	 */
	public ImageAttributesPanel() {
		super();
		initialize();
		updateComponentsFromAttribs();
	}

	@Override
	public void updateComponentsFromAttribs() {
		if (attribs.containsKey("src")) {
			imgUrlField.setText(attribs.get("src").toString());
		}

		if (attribs.containsKey("alt")) {
			altTextCB.setSelected(true);
			altTextField.setEditable(true);
			altTextField.setText(attribs.get("alt").toString());
		} else {
			altTextCB.setSelected(false);
			altTextField.setEditable(false);
		}

		if (attribs.containsKey("width")) {
			widthCB.setSelected(true);
			widthField.setEnabled(true);
			try {
				widthField.getModel().setValue(attribs.get("width"));
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			widthCB.setSelected(false);
			widthField.setEnabled(false);
		}

		if (attribs.containsKey("height")) {
			heightCB.setSelected(true);
			heightField.setEnabled(true);
			try {
				heightField.getModel().setValue(attribs.get("height"));
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			heightCB.setSelected(false);
			heightField.setEnabled(false);
		}

		if (attribs.containsKey("hspace")) {
			hSpaceCB.setSelected(true);
			hSpaceField.setEnabled(true);
			try {
				hSpaceField.getModel().setValue(attribs.get("hspace"));
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			hSpaceCB.setSelected(false);
			hSpaceField.setEnabled(false);
		}

		if (attribs.containsKey("vspace")) {
			vSpaceCB.setSelected(true);
			vSpaceField.setEnabled(true);
			try {
				vSpaceField.getModel().setValue(attribs.get("vspace"));
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			vSpaceCB.setSelected(false);
			vSpaceField.setEnabled(false);
		}

		if (attribs.containsKey("border")) {
			borderCB.setSelected(true);
			borderField.setEnabled(true);
			try {
				borderField.getModel().setValue(attribs.get("border"));
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			borderCB.setSelected(false);
			borderField.setEnabled(false);
		}

		if (attribs.containsKey("align")) {
			alignCB.setSelected(true);
			alignCombo.setEnabled(true);
			alignCombo.setSelectedItem(attribs.get("align"));
		} else {
			alignCB.setSelected(false);
			alignCombo.setEnabled(false);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void updateAttribsFromComponents() {
		attribs.put("src", imgUrlField.getText());

		if (altTextCB.isSelected()) {
			attribs.put("alt", altTextField.getText());
		} else {
			attribs.remove("alt");
		}

		if (widthCB.isSelected()) {
			attribs.put("width", widthField.getModel().getValue().toString());
		} else {
			attribs.remove("width");
		}

		if (heightCB.isSelected()) {
			attribs.put("height", heightField.getModel().getValue().toString());
		} else {
			attribs.remove("height");
		}

		if (vSpaceCB.isSelected()) {
			attribs.put("vspace", vSpaceField.getModel().getValue().toString());
		} else {
			attribs.remove("vspace");
		}

		if (hSpaceCB.isSelected()) {
			attribs.put("hspace", hSpaceField.getModel().getValue().toString());
		} else {
			attribs.remove("hspace");
		}

		if (borderCB.isSelected()) {
			attribs.put("border", borderField.getModel().getValue().toString());
		} else {
			attribs.remove("border");
		}

		if (alignCB.isSelected()) {
			attribs.put("align", alignCombo.getSelectedItem().toString());
		} else {
			attribs.remove("align");
		}
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		setLayout(new GridBagLayout());
		//setSize(365, 188);
		imgUrlLabel = new JLabel(i18n.str("image_url"));
		add(imgUrlLabel, new GBC("0,0, anchor W, ins 0 0 5 5"));
		add(getImgUrlField(), new GBC("0,1,wx 1.0, width 1, anchor W, ins 0 0 5 0"));
		add(getAltTextCB(), new GBC("1,0,anchor W, ins 0 0 10 5"));
		add(getAltTextField(), new GBC("1,1,fill H, wx 1.0, ins 0 0 10 0, width 1, anchor W"));
		add(getAttribPanel(), new GBC("2,0, width 2, anchor W"));
		add(getSpacerPanel(), new GBC("3,0,fill H, with 2, anchor W, wy 1.0"));

		TextEditPopupManager popupMan = TextEditPopupManager.getInstance();
		popupMan.registerJTextComponent(imgUrlField);
		popupMan.registerJTextComponent(altTextField);
	}

	/**
	 * This method initializes altTextCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getAltTextCB() {
		if (altTextCB == null) {
			altTextCB = new JCheckBox();
			altTextCB.setText(i18n.str("alt_text"));
			altTextCB.addItemListener((ItemEvent e) -> {
				altTextField.setEditable(altTextCB.isSelected());
			});
		}
		return altTextCB;
	}

	/**
	 * This method initializes widthCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getWidthCB() {
		if (widthCB == null) {
			widthCB = new JCheckBox();
			widthCB.setText(i18n.str("width"));
			widthCB.addItemListener((ItemEvent e) -> {
				widthField.setEnabled(widthCB.isSelected());
			});
		}
		return widthCB;
	}

	/**
	 * This method initializes heightCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getHeightCB() {
		if (heightCB == null) {
			heightCB = new JCheckBox();
			heightCB.setText(i18n.str("height"));
			heightCB.addItemListener((ItemEvent e) -> {
				heightField.setEnabled(heightCB.isSelected());
			});
		}
		return heightCB;
	}

	/**
	 * This method initializes borderCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getBorderCB() {
		if (borderCB == null) {
			borderCB = new JCheckBox();
			borderCB.setText(i18n.str("border"));
			borderCB.addItemListener((ItemEvent e) -> {
				borderField.setEnabled(borderCB.isSelected());
			});
		}
		return borderCB;
	}

	/**
	 * This method initializes widthField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getWidthField() {
		if (widthField == null) {
			widthField = new JSpinner(new SpinnerNumberModel(1, 1, 999, 1));
			//widthField.setColumns(4);
		}
		return widthField;
	}

	/**
	 * This method initializes heightField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getHeightField() {
		if (heightField == null) {
			heightField = new JSpinner(new SpinnerNumberModel(1, 1, 999, 1));
			//heightField.setColumns(4);
		}
		return heightField;
	}

	/**
	 * This method initializes borderField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getBorderField() {
		if (borderField == null) {
			borderField = new JSpinner(new SpinnerNumberModel(1, 0, 999, 1));
			//borderField.setColumns(4);
		}
		return borderField;
	}

	/**
	 * This method initializes vSpaceCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getVSpaceCB() {
		if (vSpaceCB == null) {
			vSpaceCB = new JCheckBox();
			vSpaceCB.setText(i18n.str("vspace"));
			vSpaceCB.addItemListener((ItemEvent e) -> {
				vSpaceField.setEnabled(vSpaceCB.isSelected());
			});
		}
		return vSpaceCB;
	}

	/**
	 * This method initializes hSpaceCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getHSpaceCB() {
		if (hSpaceCB == null) {
			hSpaceCB = new JCheckBox();
			hSpaceCB.setText(i18n.str("hspace"));
			hSpaceCB.addItemListener((ItemEvent e) -> {
				hSpaceField.setEnabled(hSpaceCB.isSelected());
			});
		}
		return hSpaceCB;
	}

	/**
	 * This method initializes alignCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getAlignCB() {
		if (alignCB == null) {
			alignCB = new JCheckBox();
			alignCB.setText(i18n.str("align"));
			alignCB.addItemListener((ItemEvent e) -> {
				alignCombo.setEnabled(alignCB.isSelected());
			});
		}
		return alignCB;
	}

	/**
	 * This method initializes vSpaceField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getVSpaceField() {
		if (vSpaceField == null) {
			vSpaceField = new JSpinner(new SpinnerNumberModel(1, 1, 999, 1));
			//vSpaceField.setColumns(4);
		}
		return vSpaceField;
	}

	/**
	 * This method initializes hSpaceField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getHSpaceField() {
		if (hSpaceField == null) {
			hSpaceField = new JSpinner(new SpinnerNumberModel(1, 1, 999, 1));
		}
		return hSpaceField;
	}

	/**
	 * This method initializes alignCombo
	 *
	 * @return javax.swing.JComboBox
	 */
	@SuppressWarnings("unchecked")
	private JComboBox getAlignCombo() {
		if (alignCombo == null) {
			alignCombo = new JComboBox(ALIGNMENTS);
		}
		return alignCombo;
	}

	/**
	 * This method initializes imgUrlField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getImgUrlField() {
		if (imgUrlField == null) {
			imgUrlField = new JTextField();
		}
		imgUrlField.setColumns(32);
		return imgUrlField;
	}

	/**
	 * This method initializes altTextField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getAltTextField() {
		if (altTextField == null) {
			altTextField = new JTextField();
		}
		return altTextField;
	}

	/**
	 * This method initializes attribPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getAttribPanel() {
		if (attribPanel == null) {
			attribPanel = new JPanel();
			attribPanel.setLayout(new GridBagLayout());

			GBC gbc00 = new GBC(0, 0);
			gbc00.anchor = GridBagConstraints.WEST;
			gbc00.insets = new Insets(0, 0, 5, 5);
			attribPanel.add(getWidthCB(), gbc00);

			GBC gbc10 = new GBC(1, 0);
			gbc10.anchor = GridBagConstraints.WEST;
			gbc10.insets = new Insets(0, 0, 10, 5);
			attribPanel.add(getHeightCB(), gbc10);

			GBC gbc20 = new GBC(2, 0);
			gbc20.anchor = GridBagConstraints.WEST;
			gbc20.insets = new Insets(0, 0, 10, 5);
			attribPanel.add(getBorderCB(), gbc20);

			GBC gbc01 = new GBC(0, 1, GridBagConstraints.NONE);
			gbc01.anchor = GridBagConstraints.WEST;
			gbc01.insets = new Insets(0, 0, 5, 10);
			gbc01.weightx = 0.0;
			attribPanel.add(getWidthField(), gbc01);

			GBC gbc11 = new GBC(1, 1, GridBagConstraints.NONE);
			gbc11.anchor = GridBagConstraints.WEST;
			gbc11.insets = new Insets(0, 0, 10, 10);
			gbc11.weightx = 0.0;
			attribPanel.add(getHeightField(), gbc11);

			GBC gbc21 = new GBC(2, 1, GridBagConstraints.NONE);
			gbc21.anchor = GridBagConstraints.WEST;
			gbc21.insets = new Insets(0, 0, 10, 10);
			gbc21.weightx = 0.0;
			attribPanel.add(getBorderField(), gbc21);

			GBC gbc02 = new GBC(0, 2);
			gbc02.anchor = GridBagConstraints.WEST;
			gbc02.insets = new Insets(0, 0, 5, 5);
			attribPanel.add(getVSpaceCB(), gbc02);

			GBC gbc12 = new GBC(1, 2);
			gbc12.anchor = java.awt.GridBagConstraints.WEST;
			gbc12.insets = new java.awt.Insets(0, 0, 10, 5);
			attribPanel.add(getHSpaceCB(), gbc12);

			GBC gbc22 = new GBC(2, 2);
			gbc22.anchor = GridBagConstraints.WEST;
			gbc22.insets = new Insets(0, 0, 10, 5);
			attribPanel.add(getAlignCB(), gbc22);

			GBC gbc03 = new GBC(0, 3, GridBagConstraints.NONE);
			gbc03.anchor = GridBagConstraints.WEST;
			gbc03.insets = new Insets(0, 0, 5, 0);
			gbc03.weightx = 0.0;
			attribPanel.add(getVSpaceField(), gbc03);

			GBC gbc13 = new GBC(1, 3, GridBagConstraints.NONE);
			gbc13.anchor = GridBagConstraints.WEST;
			gbc13.insets = new Insets(0, 0, 10, 0);
			gbc13.weightx = 0.0;
			attribPanel.add(getHSpaceField(), gbc13);

			GBC gbc23 = new GBC(2, 3, GridBagConstraints.NONE);
			gbc23.anchor = GridBagConstraints.WEST;
			gbc23.insets = new Insets(0, 0, 10, 0);
			gbc23.weightx = 1.0;
			attribPanel.add(getAlignCombo(), gbc23);
		}
		return attribPanel;
	}

	/**
	 * This method initializes spacerPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getSpacerPanel() {
		if (spacerPanel == null) {
			spacerPanel = new JPanel();
		}
		return spacerPanel;
	}

}
